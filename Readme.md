# hxd

**Hxd is a simple hex viewer for viewing byte sections from a file 
or the option to compare sections of files**

### Installation

- requires `cargo` to be installed
- clone the repository 
- build with `cargo build --release` to find the binary in `target/hxd` 
  - or install locally whith `cargo install -path .` (will install into _~/.cargo/bin/_ make sure its in your PATH)

### Usage
- see `hxd -h`
- example: 
~~~sh
hxd file1 -l8 -d file2 bytes -s 40 -c 100

# reads 100 bytes from two files while skipping the first 40
# and displays them side by side in an 8 column wide view
~~~

### baby steps

- [x] read file
- [x] read certain length at position from file
- [x] pretty print section
- [x] read sections from twwo files
- [x] compare two sections | find overlapping similarities
- [x] pretty print two sections
- [ ] search bytes string in section (boyer moore) 

- [ ] interactive (maybe)





