mod filesystem;
mod inputs;
mod printing;

use crate::filesystem::*;
use crate::inputs::*;
use crate::printing::*;
use clap::Parser;

fn main() {
    let args = HxdArgs::parse();
    let (count, skip) = match args.mode {
        Mode::Bytes { count, skip } => (count, skip),
        Mode::File => (0, 0),
    };
    let section = read_section(args.file, count, skip);
    if let Some(diff_file) = args.diff_file {
        let diff_section = read_section(diff_file, count, skip);

        print_section_diff(
            section.unwrap(),
            diff_section.unwrap(),
            skip,
            args.linelength,
        );
    } else {
        print_section(section.unwrap(), skip, args.linelength);
    }
}
