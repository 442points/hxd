use clap::{arg, command, Parser, Subcommand};

#[derive(Subcommand, Debug)]
pub enum Mode {
    /// standalone option, whole file will be read
    File,
    /// only read some bytes from a file, optionally from an offset
    Bytes {
        /// number of bytes to read from file
        #[arg(short)]
        count: usize,
        /// Number of bytes to skip from beginning of file
        #[arg(short, default_value_t = 0)]
        skip: usize,
    },
}

#[derive(Parser, Debug)]
#[command(about, long_about = None)]
pub struct HxdArgs {
    /// File to use
    pub file: String,
    /// Number of bytes to print
    #[command(subcommand)]
    pub mode: Mode,
    /// File to show alongside for comparison, output will be truncated to the shorter of the two if it reaches end of file
    #[arg(short)]
    pub diff_file: Option<String>,
    /// Number of bytes ber line
    #[arg(short, default_value_t = 16)]
    pub linelength: u8,
}
