use std::{
    fs::File,
    io::{self, BufReader, Read, Seek},
};

pub fn read_section(file: String, amount: usize, drop: usize) -> io::Result<Vec<u8>> {
    let mut file = File::open(file)?;
    file.seek(std::io::SeekFrom::Start(drop as u64))?;
    let reader = BufReader::new(file);
    let items: io::Result<Vec<u8>> = if amount == 0 {
        reader.bytes().collect()
    } else {
        reader.bytes().take(amount).collect()
    };
    items
}
