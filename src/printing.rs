use colored::Colorize;

#[derive(Debug)]
pub enum Diffed<T> {
    Same(T),
    Different(T, T),
}

#[derive(Debug)]
pub enum ByteString<'a> {
    Diff(&'a [Diffed<u8>]),
    Full(&'a [u8]),
}

pub fn stringify_diffed_line(line: &[Diffed<u8>]) -> (String, String) {
    let (l1, l2): (Vec<u8>, Vec<u8>) = line
        .iter()
        .map(|x| match x {
            Diffed::Same(x) => (*x, *x),
            Diffed::Different(x, y) => (*x, *y),
        })
        .unzip();
    (
        stringify_line(l1.as_slice(), |i, _b| l1[i] == l2[i]),
        stringify_line(l2.as_slice(), |i, _b| l1[i] == l2[i]),
    )
}

pub fn stringify_line(line: &[u8], marker: impl Fn(usize, u8) -> bool) -> String {
    let stringify_single_line = |mut acc: String, (i, b): (usize, u8)| {
        let mut str = format!("{:02x} ", b);
        if marker(i, b) {
            str = str.on_yellow().to_string();
        }
        acc.push_str(str.as_str());
        acc
    };

    line.iter()
        .map(|x| *x)
        .enumerate()
        .fold("".to_string(), stringify_single_line)
}

pub fn build_line_string(val: ByteString, line_index: usize, linewidth: u8) -> String {
    let line_number = format!("{:08x}: ", line_index,);

    let build_line = |byte_visual: String, text: String| {
        format!(
            "{}{}{}\n",
            line_number.blue(),
            byte_visual.red(),
            text.green()
        )
    };

    match val {
        ByteString::Full(line) => {
            let mut byte_visual = stringify_line(&line, |_, _| false);
            let text = pp_bytes_as_string(line);
            let len = line.len();
            if len < linewidth as usize {
                byte_visual.push_str("   ".to_string().repeat(linewidth as usize - len).as_str());
            }
            build_line(byte_visual, text)
        }
        ByteString::Diff(line) => {
            let (l1, l2): (Vec<u8>, Vec<u8>) = line
                .iter()
                .map(|x| match x {
                    Diffed::Same(x) => (*x, *x),
                    Diffed::Different(x, y) => (*x, *y),
                })
                .unzip();
            let (mut visual1, mut visual2) = stringify_diffed_line(line);
            let text1 = pp_bytes_as_string(l1.as_slice());
            let text2 = pp_bytes_as_string(l2.as_slice());
            let len1 = l1.len();
            let len2 = l2.len();
            if len1 < linewidth as usize {
                visual1.push_str("   ".to_string().repeat(linewidth as usize - len1).as_str());
            }
            if len2 < linewidth as usize {
                visual2.push_str("   ".to_string().repeat(linewidth as usize - len2).as_str());
            }
            format!(
                "{}{}",
                build_line(visual1, text1),
                build_line(visual2, text2).dimmed().underline()
            )
        }
    }
}

pub fn print_section(section: Vec<u8>, starting_adress: usize, linewidth: u8) {
    section
        .chunks(linewidth as usize)
        .enumerate()
        .map(|(i, val)| {
            build_line_string(
                ByteString::Full(val),
                starting_adress as usize + i * linewidth as usize,
                linewidth,
            )
        })
        .for_each(|s| print!("{}", s));
}
pub fn print_section_diff(s1: Vec<u8>, s2: Vec<u8>, starting_adress: usize, linewidth: u8) {
    s1.iter()
        .zip(s2.iter()) // this zip call likely truncates the diff to the length of the shorter file
        .map(|(v1, v2)| {
            if v1 == v2 {
                Diffed::Same(*v1)
            } else {
                Diffed::Different(*v1, *v2)
            }
        })
        .collect::<Vec<Diffed<u8>>>()
        .chunks(linewidth as usize)
        .enumerate()
        .map(|(i, line)| {
            build_line_string(
                ByteString::Diff(line),
                starting_adress as usize + i * linewidth as usize,
                linewidth,
            )
        })
        .for_each(|s| print!("{}", s));
}

pub fn pp_bytes_as_string(bytes: &[u8]) -> String {
    let mut bytes = bytes.to_owned();
    bytes.iter_mut().for_each(|b| {
        if !b.is_ascii() || b.is_ascii_whitespace() || b.is_ascii_control() {
            *b = b'.';
        }
    });
    String::from_utf8_lossy(&bytes).to_string()
}
